from .models import Contact
from .serializers import ContactSerializer
from rest_framework import generics

class ContactListCreate(generics.ListCreateAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

class ContactListRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    lookup_field = "id"

    # def get_queryset(self):
    #     return self.queryset.filter()