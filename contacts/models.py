from django.db import models

# Create your models here.

class Contact(models.Model):
    name = models.CharField(max_length=100, blank=True)
    address = models.CharField(max_length=100, default='', blank=True)
    age = models.IntegerField(default=None, blank=True, null=True)
    email = models.EmailField(default='', blank=True)
    phone = models.IntegerField(default=None, blank=True, null=True)
    details = models.CharField(max_length=1000, default='', blank=True)