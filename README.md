# Django backend contact list
A simple CRUD API with Django, that handles contacts

This backend is meant to be used with the corresponding React frontend, which can be found [here](https://gitlab.com/djangorestwithreactfront/reactfrontend)


# Getting started

## Development mode
* Clone this repository
* Run "python manage.py makemigrations"
* Run "python manage.py migrate"
* Then run "python manage.py runserver"


# Prerequisites
* Python 3.x
* Django 2.x
* Django-Restframework
* Django-CORS-headers


# Authors
* Jens CT Hetland